<?php

function createFileWithSum(string $pathToFiles): void {
    // Даны два текстовых файла 1.txt и 2.txt. Они находятся в директории $pathToFiles
    // Каждый файл содержит по n целых чисел, располагающихся на отдельных строках.
    // Необходимо вычислить суммы чисел из двух файлов на соответствующих строках и записать их в файл 3.txt.
    // Файл 3.txt необходимо создать в директории $pathToFiles

    //vendor\bin\phpunit --filter testSuccessCreateFileWithSum  TasksTest.php .

    if (!is_dir($pathToFiles)) {return;}

    $array1 = $array = file("$pathToFiles/1.txt");
	$array2 = $array = file("$pathToFiles/2.txt");
	$array3 = [];

	$arraylen = count($array1);

	for ($i=0; $i < $arraylen; $i++) { 
		$num1 = (int)$array1[$i];
		$num2 = (int)$array2[$i];	

		$sum = $num1 + $num2;

		$array3[$i]	= $sum;
	}

	file_put_contents("$pathToFiles/3.txt", implode(PHP_EOL,$array3));
}
