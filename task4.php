<?php
$array = createDeepArrayOfNumbers(3);
var_dump($array);
function createDeepArrayOfNumbers(int $deep): array {
    // Реализуйте функцию, которая случайным образом создает массив из X элементов,
    // состоящий из случайных целых чисел и из массивов целых чисел.
    // Глубина массива - $deep.
    // X должно быть больше 5 и меньше 10
    // Числа должны находиться в диапозоне от 10 до 10000

    // vendor\bin\phpunit --filter testCreateDeepArrayOfNumbers  TasksTest.php .

    $X = rand(6,9); 
    $element = rand(10,10000);
    for ($j=0; $j <$deep; $j++) { 
        $arr = createArray($element, $X);
        $element = $arr;
    }
    return $arr;
}

function createArray($element, int $X){
    for ($i=0; $i < $X; $i++) 
    { 
        $Y = rand(10,10000);
        $arr[$i] = $Y;
    }
    $arr[$X-1] = $element;
    return $arr;
}    


function calculateSum(array $deepArrayOfNumbers): int {
    // Напишите функцию которая вычисляет сумму чисел всех элементов и подэлементов структуры,
    // создаваемой функцией createDeepArrayOfNumbers.

    // vendor\bin\phpunit --filter testCalculateSum  TasksTest.php .

    $sum = gotolevel($deepArrayOfNumbers);
    return $sum;
}

function gotolevel(array $arr)
{
    $sum = 0;
    foreach ($arr as $key) {
        if(is_array($key))
        {
            $sum = $sum + gotolevel($key);
        }
        else 
        {
            $sum = $sum + $key;
        }
    }
    return $sum;
}
