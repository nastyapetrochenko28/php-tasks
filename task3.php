<?php

function rewriteJsonFile(string $pathToJsonFile, string $key, $value): void {
    // JSON файл лежит по пути $pathToJsonFile. Необходимо получить содержимое этого файла,
    // добавить в него поле $key со значением $value и перезаписать.

    // vendor\bin\phpunit --filter testRewriteJsonFile  TasksTest.php .

    $file = $pathToJsonFile;
	$filecontent = file_get_contents($file, true);

	$array = json_decode($filecontent, true);
   $array += [$key=>$value];

    $filecontent = json_encode($array);
    file_put_contents($file, $filecontent);
}
